import axios from 'axios';

let baseURL = process.env.baseURL || "https://lossobrevivientesapp.com/los/api";

let instance = axios.create({
	baseURL
});

export const HTTP = instance;